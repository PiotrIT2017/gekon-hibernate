package engine.pp.Gekonh;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


@SpringBootApplication
public class GekonHApplication {

	String item;

	public String openfilewithVote() {
		String csvFile = "./data/resolution-list.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		ArrayList<String> resolutions = new ArrayList<String>();
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				// use comma as separator
				String[] law = line.split(cvsSplitBy);
				System.out.println(" [law= " + law[0] + "]");
				/*for (int i = 0; i <= law.length-1; i++) {
					String resolution[] = law[2].split(cvsSplitBy);
					String resolutionslist = resolution[i] + resolution[i + 1];
					resolutions.add(resolutionslist);
				}*/
				Iterator itr1 = resolutions.iterator();
				while (itr1.hasNext()) {
					Object resolutionslist = itr1.next();
					int resolutionslists = (int) resolutionslist;
					item = resolutionslist.toString();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return item;
	}

	public static void main(String[] args) {

		GekonHApplication votersystem=new GekonHApplication();
		Quest vote=new Quest();
		vote.setItem(votersystem.openfilewithVote());

		Configuration cfg = new Configuration().configure().addAnnotatedClass(Quest.class);
		StandardServiceRegistry reg = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties()).build();
		SessionFactory sf = cfg.buildSessionFactory(reg);
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
		session.save(vote);
		tx.commit();

		SpringApplication.run(GekonHApplication.class, args);
	}

}
