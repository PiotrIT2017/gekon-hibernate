package engine.pp.Gekonh;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Quest {

    @Id
    private String item;

    public void setItem(String Item) {
        item = Item;
    }

    public String getItem() {
        return item;
    }
}
